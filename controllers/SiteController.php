<?php

class SiteController
{
    public function actionIndex()
    {
        $name = '';
        $text = '';
        $errors = false;
        if (isset($_POST['submit']))
        {
            $name = Validate::clean($_POST['name']);
            $text = Validate::clean($_POST['text']);

            if (!empty($name) && !empty($text))
            {
                if (!Validate::checkLength($name, 2, 50))
                    $errors[] = 'Имя не должно быть короче 2-х символов';
                if (!Validate::checkLength($text, 1, 5000))
                    $errors[] = 'Длина текста должна быть от 1 до 5000 символов';
            }
            else
                $errors[] = 'Заполните все поля';

            if ($errors == false)
            {
                if (Post::addPost($name, $text))
                    header("Location: " . $_SERVER['REQUEST_URI']);
                else
                    die('При добавлении записи возникла внутренняя ошибка');
            }
        }

        $posts = Post::getPostsForMainPage();

        $idMostPopularPosts = array();
        foreach (Comment::getIdsMostPopularPost(5) as $idMostPopularPost)
        {
            $idMostPopularPosts[] = array_search($idMostPopularPost, array_column($posts, 'id'));
        }

        require_once(ROOT . '/views/site/index.php');

        return true;
    }
}
?>