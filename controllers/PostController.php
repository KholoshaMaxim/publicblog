<?php

class PostController
{
    public function actionView($id)
    {
        $postId = '';
        $name = '';
        $text = '';
        $errors = false;

        if (isset($_POST['submit']))
        {
            $postId = $_POST['postId'];
            $name = Validate::clean($_POST['name']);
            $text = Validate::clean($_POST['text']);

            if (!empty($name) && !empty($text))
            {
                if (!Validate::checkLength($name, 2, 50))
                    $errors[] = 'Имя не должно быть короче 2-х символов';
                if (!Validate::checkLength($text, 1, 5000))
                    $errors[] = 'Длина текста должна быть от 1 до 5000 символов';
            }
            else
                $errors[] = 'Заполните все поля';

            if ($errors == false)
            {
                if (Comment::addComment($postId, $name, $text))
                    header("Location: ".$_SERVER['REQUEST_URI']);
                else
                    die('При добавлении комментария возникла внутренняя ошибка');
            }
        }

        if (!$post = Post::getPostById($id))
            require_once(ROOT. '/views/layouts/404.php');
        else
        {
            $post['comments'] = Comment::getCommentsByPostId($id);
            require_once(ROOT. '/views/post/view.php');
        }

        return true;
    }
}
?>