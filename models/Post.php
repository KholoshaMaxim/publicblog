<?php 

class Post
{
    public static function getPostsForMainPage()
    {
        $db = Db::getConnection();

        $result = $db->query("SELECT p.*, c.countComments FROM post p LEFT OUTER JOIN "
            . "(SELECT post_id, COUNT(*) as 'countComments' FROM comment c GROUP BY post_id ORDER BY post_id) "
            . "c ON p.id = c.post_id ORDER BY p.created_at DESC ");

        $posts = array();
        $i = 0;
        while ($row = $result->fetch()) {
            $posts[$i]['id'] = $row['id'];
            $posts[$i]['name'] = $row['name'];
            if (strlen($row['text']) > 100)
                $posts[$i]['text'] = mb_strimwidth($row['text'], 0, 103, "...");
            else
                $posts[$i]['text'] = $row['text'];
            $posts[$i]['created_at'] = $row['created_at'];
            $posts[$i]['countComments'] = $row['countComments'] ? $row['countComments'] : 0;
            $posts[$i]['links']['self'] = "/post/" . $posts[$i]['id'];
            $i++;
        }
        return $posts;
    }

    public static function addPost($name, $text)
    {
        try
        {
            $db = Db::getConnection();

            $sql = 'INSERT INTO post (name, text) VALUES (:name, :text)';

            $result = $db->prepare($sql);
            $result->bindParam(':name', $name, PDO::PARAM_STR);
            $result->bindParam(':text', $text, PDO::PARAM_STR);

            return $result->execute();
        }
        catch (Exception $exception)
        {
            return false;
        }
    }

    public static function getPostById($id)
    {
        try
        {
            $db = Db::getConnection();

            $sql = 'SELECT * FROM post WHERE id = :id';

            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            return $result->fetch();
        } catch (Exception $exception)
        {
            return false;
        }
    }
}
?>