<?php 

class Comment
{
    public static function getCommentsByPostId($postId)
    {
        try
        {
            $db = Db::getConnection();

            $sql = 'SELECT * FROM comment WHERE post_id = :post_id ORDER BY id DESC';

            $result = $db->prepare($sql);
            $result->bindParam(':post_id', $postId, PDO::PARAM_INT);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            $comments = array();
            $i = 0;
            while ($row = $result->fetch()) {
                $comments[$i]['id'] = $row['id'];
                $comments[$i]['name'] = $row['name'];
                $comments[$i]['text'] = $row['text'];
                $comments[$i]['created_at'] = $row['created_at'];
                $i++;
            }

            return $comments;
        } catch (Exception $exception)
        {
            return array();
        }
    }

    public static function addComment($postId, $name, $text)
    {
        try
        {
            $db = Db::getConnection();

            $sql = 'INSERT INTO comment (post_id, name, text) VALUES (:post_id, :name, :text)';

            $result = $db->prepare($sql);
            $result->bindParam(':post_id', $postId, PDO::PARAM_INT);
            $result->bindParam(':name', $name, PDO::PARAM_STR);
            $result->bindParam(':text', $text, PDO::PARAM_STR);

            return $result->execute();
        }
        catch (Exception $exception)
        {
            return false;
        }
    }

    public static function getIdsMostPopularPost($count)
    {
        try
        {
            $db = Db::getConnection();

            $sql = 'SELECT post_id, COUNT(post_id) as countComments '
                . 'FROM comment GROUP BY post_id ORDER BY countComments DESC LIMIT :count';

            $result = $db->prepare($sql);
            $result->bindParam(':count', $count, PDO::PARAM_INT);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();

            $mostPopularPosts = array();
            while ($row = $result->fetch())
            {
                $mostPopularPosts[] = $row['post_id'];
            }

            return $mostPopularPosts;
        }
        catch (Exception $exception)
        {
            return array();
        }
    }
}
?>