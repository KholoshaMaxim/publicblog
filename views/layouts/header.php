<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Главная</title>
        <link rel="stylesheet" href="/template/css/bootstrap.min.css">
        <link rel="stylesheet" href="/template/css/font-awesome.min.css">
        <link rel="stylesheet" href="/template/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="/template/css/main.css">
    </head><!--/head-->

    <body>
