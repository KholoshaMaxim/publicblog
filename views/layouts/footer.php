	    <footer id="footer"><!--Footer-->
	        <div class="footer-bottom">
	            <div class="container">
	                <div class="row">
	                    <p class="pull-left">Copyright © 2017</p>
	                    <p class="pull-right">Холоша Максим</p>
	                </div>
	            </div>
	        </div>
	    </footer><!--/Footer-->

        <script type="text/javascript" src="/template/js/jquery-3.2.1.min.js"></script>
        <script src="/template/js/bootstrap.min.js"></script>
        <script src="/template/owl-carousel/owl.carousel.min.js"></script>
        <script src="/template/js/main.js"></script>
    </body>
</html>