<?php include ROOT.'/views/layouts/header.php' ?>
    <section>
        <div class="container">
            <div class="row">
                <a href="/">На главную</a>
                <div class="col-sm-offset-1">
                    <div class="view_post">
                        <?php $i = count($post['comments']); ?>
                        <h3><?= $post['name']?> (<?= $post['created_at']?>)</h3>
                        <p><?= $post['text']?></p>
                        <br>
                        <p>Комментарии:</p>
                        <?php if (count($post['comments'])):?>
                            <?php foreach ($post['comments'] as $comment):?>
                                <h4><?= $i-- . '. ' . $comment['name'] . ' (' . $comment['created_at']. ')'?></h4>
                                <p><?= $comment['text']?></p>
                                <hr>
                            <?php endforeach; ?>
                        <?php else: ?>
                            <p>Коментариев еще нет. Будь первым!</p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-offset-1">
                    <div class="add_form">
                        <?php if (isset($errors) && is_array($errors)): ?>
                            <ul>
                                <?php foreach ($errors as $error): ?>
                                    <li class="li_error"> <?php echo $error; ?></li>
                                <?php endforeach; ?>
                            </ul>
                        <?php endif; ?>
                        <h2>Добавление комментария</h2>
                        <form action="#" method="post">
                            <input type="hidden" name="postId" value="<?= $post['id'] ?>">
                            <p>Имя:</p>
                            <input type="text" name="name" class="input" placeholder="Имя" value="<?= $name; ?>">
                            <p>Текст:</p>
                            <textarea type="text" name="text" rows="10" placeholder="Текст"><?= $text; ?></textarea>
                            <br>
                            <input type="submit" name="submit" class="btn btn-primary" value="Добавить">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php include ROOT.'/views/layouts/footer.php' ?>

