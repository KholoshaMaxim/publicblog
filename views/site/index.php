<?php include ROOT.'/views/layouts/header.php' ?>
        <section>
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <div id="carousel">
                            <?php foreach ($idMostPopularPosts as $idMostPopularPost):?>
                                <div class="carousel-element">
                                    <p><?= $posts[$idMostPopularPost]['name'] . ' (' . $posts[$idMostPopularPost]['countComments'] . ')'?></p>
                                    <p><?= $posts[$idMostPopularPost]['created_at']?></p>
                                    <p><?= $posts[$idMostPopularPost]['text']?></p>
                                    <a href="<?= $posts[$idMostPopularPost]['links']['self']?>">Открыть полностью</a>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <p class="center-text"><a href="#" id="js-prev">Назад</a>
                            <a href="#" id="js-next">Вперед</a></p>
                    </div>
                    <div class="col-sm-offset-4">
                        <div class="add_form">
                            <h2>Создание записи</h2>
                            <?php if (isset($errors) && is_array($errors)): ?>
                                <ul>
                                    <?php foreach ($errors as $error): ?>
                                        <li class="li_error"> <?php echo $error; ?></li>
                                    <?php endforeach; ?>
                                </ul>
                            <?php endif; ?>
                            <form action="#" method="post">
                                <p>Имя:</p>
                                <input type="text" name="name" class="input" placeholder="Введите имя" value="<?= $name; ?>">
                                <p>Текст:</p>
                                <textarea type="text" name="text" rows="10" placeholder="Введите текст"><?= $text; ?></textarea>
                                <br>
                                <input type="submit" name="submit" class="btn btn-primary" value="Создать">
                            </form>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-offset-4">
                        <?php foreach($posts as $post):?>
                            <div>
                                <p><?= $post['name'] . ' (' . $post['created_at'] . ')'?></p>
                                <p><?= $post['text']?></p>
                                <p>Количество комментариев: <?= $post['countComments'] ?></p>
                                <a href="<?= $post['links']['self']?>">Открыть полностью</a>
                                <hr>
                            </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </section>

        <?php include ROOT.'/views/layouts/footer.php' ?>
